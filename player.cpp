#include <player.h>
#include <QGraphicsRectItem>
#include <QGraphicsScene>
#include <QGraphicsPathItem>
#include <QPen>


player::player(QObject *parent, QPoint position, QGraphicsScene &scene)
    :QObject(parent),_position(position),_kierunek(2,0)
{
    _gracz = new QGraphicsRectItem;
    _gracz->setRect(0,0,_rozmiar,_rozmiar);
    scene.addItem(_gracz);
    _gracz->setPos(_position);
    _gracz->setPen(QPen(Qt::green));

    QPainterPath path;
    _mypath = new QGraphicsPathItem(_gracz);
    _mypath->setPath(path);
    _mypath->setPos(QPoint(0,_rozmiar/2));
    _mypath->setPen(QPen(Qt::green));
    scene.addItem(_mypath);

}
void player::set_kierunek(QPoint kierunek)
{
    _kierunek = kierunek;
}

void player::przesun()
{
    QPainterPath _path = _mypath->path();
    _path.moveTo(0,0);
    _position += _kierunek;
    _previous -= _kierunek;
    _gracz->setPos(_position);
    _path.lineTo(_previous);
    _mypath->setPath(_path);
}

