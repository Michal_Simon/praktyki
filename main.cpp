#include "game.h"
#include "tron.h"
#include <QApplication>
#include <QGraphicsScene>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QGraphicsScene scene;
    game game(nullptr, scene);
    Tron w(scene);
    w.show();
    return a.exec();
}
