#include "game.h"
#include "player.h"
#include <QTimer>
#include <QGraphicsScene>

namespace {
const int PERIOD = 1;
const int DEFAULT_SPEED =50/PERIOD;
}

game::game(QObject *parent,QGraphicsScene &scene)
    :QObject(parent),_pos_start_one(0,225)
{
    _refresh = new QTimer(this);
    connect(_refresh, SIGNAL(timeout()), this, SLOT(update()));
    _refresh->start(PERIOD);

    _one = new player(this,_pos_start_one,scene);
    _one->set_kierunek(QPoint(2,0));
}

void game::update()
{
    if(_speed==DEFAULT_SPEED)
        {
           _one->przesun();
            _speed=0;
        }
    ++_speed;
}
