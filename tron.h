
#ifndef TRON_H
#define TRON_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QTimer>

class QPushButton;

//QT_BEGIN_NAMESPACE
//namespace Ui { class Tron; }
//QT_END_NAMESPACE

class Tron : public QMainWindow

{
public:
    Tron(QGraphicsScene &scene,QWidget *parent = nullptr);
    ~Tron();

private:
//    Ui::Tron *ui;
    QGraphicsScene & _scene;
    QGraphicsView * _view = nullptr;
    QPushButton * _start = nullptr;

};

#endif // TRON_H
