
#ifndef GAME_H
#define GAME_H
#include <QObject>
#include <QPoint>
class QTimer;
class player;
class QGraphicsScene;

class game : public QObject
{
    Q_OBJECT
public:
    game(QObject *parent,QGraphicsScene &scene);
    ~game() override = default;
private slots:
    void update();
private:
    QTimer * _refresh = nullptr;
    player * _one = nullptr;
    const QPoint _pos_start_one;
    int _speed = 0;
};

#endif // GAME_H
