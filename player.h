#ifndef PLAYER_H
#define PLAYER_H
#include <QObject>
#include <QPoint>
class QGraphicsRectItem;
class QGraphicsScene;
class QGraphicsPathItem;

class player: public QObject
{
    Q_OBJECT
public:
    player(QObject * parent, QPoint point, QGraphicsScene &scene);
    ~player() override = default;
    void set_kierunek(QPoint kierunek);
    void przesun();

private:
    QGraphicsRectItem * _gracz = nullptr;

    QPoint _position;
    QPoint _kierunek;

    const int _rozmiar=5;

    QGraphicsPathItem * _mypath = nullptr;
    QPoint _previous;
};

#endif // PLAYER_H
