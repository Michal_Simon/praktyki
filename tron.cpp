#include "tron.h"
#include "qpushbutton.h"
#include <QtGui>
#include <QtWidgets>
#include <QTimer>

Tron::Tron(QGraphicsScene &scene,QWidget *parent)
    : QMainWindow(parent),_scene(scene)
{
    _view = new QGraphicsView;
    _start = new QPushButton("Start",this);
    _scene.setSceneRect(0,0,500,500);
    _view->setScene(&_scene);

    setMenuWidget(_start);
    setCentralWidget(_view);
}

Tron::~Tron()
{

}


